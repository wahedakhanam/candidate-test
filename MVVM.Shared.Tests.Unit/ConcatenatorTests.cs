using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MVVM.Shared.Tests.Unit
{
    [TestClass]
    public class ConcatenatorTests
    {
        // TODO: Write a test(s) for the MVVM.Shared.Concatenator.Concatenate() method.

        
        [TestMethod]
        public static void TestConcatenate()
        {
            string[] arrayToTest = { "Model", "View", "ViewModel", "Design" };
            var expectedTestResult =  "ModelViewViewModelDesign" ;
            var actualtestResult = Concatenator.Concatenate(arrayToTest);
            Assert.AreEqual(expectedTestResult, actualtestResult);
        }
        
    }
}
