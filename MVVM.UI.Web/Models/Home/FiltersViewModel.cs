namespace MVVM.UI.Web.Models.Home
{
    public class FiltersViewModel
    {
        public string Text { get; set; } = "Please adjust filters as appropriate.";
    }
}
