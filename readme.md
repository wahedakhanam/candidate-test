# Neogic Candidate Technical Test - C#/MVVM

This test requires .NET Framework 4.6.1 and either Microsoft Visual Studio or a suitable alternative. 

## Instructions
1. Clone the repository to your own copy on Bitbucket, then clone locally. 
2. In Git or VS, create a feature branch off master called "*ft/`<YOUR INITIALS>`-candidate-test*".
3. Complete the Tasks below. Commit to your branch after each task, with a brief description.
4. When tasks are complete, merge to master, push to your BB account and e-mail the URL. 

## Tasks

1.	There is an exception when trying to load the homepage - please fix.

2.	Using *site.css*, centre-align the contents of the homepage topmost `<div>` both vertically and horizontally, and spend 5 minutes adding some polish to the overall page appearance.

3.	File *MVVM.Shared\Concatenator.cs* contains a badly implemented method `Concatenate()`. Refactor to improve efficiency.

4.	Add unit test(s) for the above to *MVVM.Shared.Tests.Unit\ConcatenatorTests.cs*.

5.	Using a Knockout.js binding, make the element `#data-textarea` show and hide based on the selection of `#select-list`.

6.	Correct the error in method `retrieveDataFromServer()` in file *MVVM.UI.Web\Scripts\Home\index.js*.

7.	Write a new KO Computed *ListBoxItemsCount()*, and bind the text of `#count-span` to it. The view */Home/Categories* should show the total number of items in the list box at any time.

8.	Using KO, adjust the `<select>` elements at URL */Home/Filters* such that they have the `multiple` attribute only if the "any" option isn't selected.
